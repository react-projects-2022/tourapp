import React, { useState, useEffect } from "react";
import Tours from "./component/Tours";
import Loading from "./component/Loading";

const url = "https://course-api.com/react-tours-project";

function App() {
  const [loading, setLoading] = useState(true);
  const [tour, setTour] = useState([]);

  // pass this function via props
  const removeTour = (id) => {
    const newTour = tour.filter((tours) => tours.id !== id);
    setTour(newTour);
  };

  const fetchTour = () => {
    setLoading(true);
    fetch(url)
      .then((resp) => resp.json())
      .then((data) => {
        setTour(data);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchTour();
  }, []);

  if (loading) {
    return (
      <main>
        <Loading />
      </main>
    );
  }
  if (tour.length === 0) {
    return (
      <main>
        <div className="title">
          <h2>No tour left</h2>
          <button className="btn" onClick={fetchTour}>
            refresh
          </button>
        </div>
      </main>
    );
  }

  return (
    <main>
      <Tours tours={tour} removeTour={removeTour} />
    </main>
  );
}

export default App;
