import React, { useState, useEffect } from "react";

function Tours({ tours, removeTour }) {
  const [readMore, setReadMore] = useState(false);
  const [search, setSearch] = useState("");
  const [filterData, setFilterData] = useState([]);

  useEffect(() => {
    setFilterData(
      tours.filter((tour) => {
        return tour.name.toLowerCase().includes(search.toLowerCase());
      })
    );
  }, [search, setFilterData, tours]);

  return (
    <section>
      <div className="title">
        <h2>our tours</h2>
        <div className="underline"></div>
        <div className="search">
          <input
            type="text"
            placeholder="Search Tours..."
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
      </div>
      <div>
        {filterData.map((tour) => {
          const { id, name, image, info, price } = tour;
          return (
            <article className="single-tour" key={id}>
              <img src={image} alt={name} />
              <footer>
                <div className="tour-info">
                  <h4>{name}</h4>
                  <h4 className="tour-price">${price}</h4>
                </div>
                <p>
                  {readMore ? info : `${info.substring(0, 200)}...`}
                  <button onClick={() => setReadMore(!readMore)}>
                    {readMore ? "Show less" : "Show More"}
                  </button>
                </p>
                <button className="delete-btn" onClick={() => removeTour(id)}>
                  Not intersted
                </button>
              </footer>
            </article>
          );
        })}
      </div>
    </section>
  );
}

export default Tours;
