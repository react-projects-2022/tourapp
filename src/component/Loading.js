import { TailSpin } from "react-loader-spinner";

function Loading() {
  return (
    <div className="loading">
      <TailSpin color="#00BFFF" height={40} width={40} />
    </div>
  );
}

export default Loading;
